       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CALCULATE-GRADE.
       AUTHOR. 62160140 PAKAWAT JAROENYING.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE  ASSIGN TO "grade.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT SCORE-FILE ASSIGN TO "avg.txt"
              ORGANIZATION   IS LINE  SEQUENTIAL.

       
       DATA DIVISION. 
       FILE SECTION.
       FD GRADE-FILE.
       01  GRADE-DETIAL.
           88 END-OF-FILE VALUE HIGH-VALUE.
           05 GRADE-ID   PIC  9(6).
           05 GRADE-NAME PIC X(50).
           05 GRADE-WEIGHT   PIC   9.
           05 GRADE-GRADE PIC X(2).
       
       FD  SCORE-FILE.
       01  SCORE-DETAIL.
           05 AVG-GRADE   PIC   9V999.
           05 AVG-SCI-GRADE  PIC   9V999.
           05 AVG-CS-GRADE   PIC   9V999.
       
       WORKING-STORAGE SECTION. 
       01  GRADE-REPORT.
       05  AVG-GRADE.
           10  TOTAL-WEIGHT   PIC   999 VALUE   ZEROS .
           10  EACH-COURSE-GRADE PIC 99V999   VALUE ZEROS .
           10  SUM-GRADE   PIC   999V999   VALUE ZEROS .
           10  AVG-GRADE   PIC   9V999 VALUE ZEROS .
       05  AVG-SCI-GRADE.
           10  TOTAL-WEIGHT   PIC   999 VALUE   ZEROS .
           10  EACH-COURSE-GRADE PIC 99V999   VALUE ZEROS .
           10  SUM-GRADE   PIC   999V999   VALUE ZEROS .
           10  AVG-GRADE   PIC   9V999 VALUE ZEROS .
       05  AVG-CS-GRADE.
           10  TOTAL-WEIGHT   PIC   999 VALUE   ZEROS .
           10  EACH-COURSE-GRADE PIC 99V999   VALUE ZEROS .
           10  SUM-GRADE   PIC   999V999   VALUE ZEROS .
           10  AVG-GRADE   PIC   9V999 VALUE ZEROS .
       01  GRADE-EVALUATE PIC   9V9 VALUE ZEROS.
 
       


       
       PROCEDURE DIVISION .
       000-BEGIN.
           PERFORM 001-OPEN-FILE THRU  001-EXIT
           PERFORM 010-REPORT THRU 010-EXIT
           GOBACK 
       .

       001-OPEN-FILE.
           OPEN INPUT GRADE-FILE 
              PERFORM UNTIL END-OF-FILE 
                    READ GRADE-FILE 
                       AT END  SET  END-OF-FILE TO TRUE
                    END-READ
                    IF NOT END-OF-FILE 
      *                 AVG-GRADE
                        PERFORM 005-CONVERT-GRADE 
                        PERFORM 005-CALCULATE-AVG-GRADE
      *                 AVG-SCI-GRADE
                        IF GRADE-ID(1:1) = "3"
                          PERFORM 005-CALCULATE-AVG-SCI-GRADE  
                        END-IF 
      *                 AVG-CS-GRADE
                        IF GRADE-ID(1:2) = "31"
                          PERFORM 005-CALCULATE-AVG-CS-GRADE  
                        END-IF 
                    ELSE 
      *                 AVG-GRADE
                       COMPUTE AVG-GRADE IN AVG-GRADE   = 
                       SUM-GRADE IN  AVG-GRADE / TOTAL-WEIGHT IN 
                       AVG-GRADE   
      *                 AVG-SCI-GRADE
                        COMPUTE AVG-GRADE IN AVG-SCI-GRADE    = 
                       SUM-GRADE IN  AVG-SCI-GRADE / TOTAL-WEIGHT IN 
                       AVG-SCI-GRADE 
      *                 AVG-SCI-GRADE
                        COMPUTE AVG-GRADE IN AVG-CS-GRADE    = 
                       SUM-GRADE IN  AVG-CS-GRADE / TOTAL-WEIGHT IN 
                       AVG-CS-GRADE 

                    END-IF
              END-PERFORM
           CLOSE GRADE-FILE 
       .
          
       001-EXIT.
           EXIT
       .
      
    
       005-CONVERT-GRADE.
           EVALUATE GRADE-GRADE 
              WHEN "A" MOVE  4.0 TO GRADE-EVALUATE
              WHEN  "B+"  MOVE  3.5 TO GRADE-EVALUATE 
              WHEN  "B"   MOVE  3 TO GRADE-EVALUATE 
              WHEN "C+"   MOVE  2.5 TO GRADE-EVALUATE 
              WHEN  "C"   MOVE  2 TO GRADE-EVALUATE 
              WHEN  "D+"  MOVE  1.5 TO GRADE-EVALUATE 
              WHEN  "D"   MOVE  1 TO GRADE-EVALUATE 
              WHEN OTHER MOVE  0  TO GRADE-EVALUATE 
           END-EVALUATE 
           EXIT 
       .
       005-CALCULATE-AVG-GRADE.
      *    หน่วยกิตคูณด้วยเกรดแต่ละรายวิชา
           COMPUTE EACH-COURSE-GRADE IN AVG-GRADE  
            = GRADE-EVALUATE * GRADE-WEIGHT 
      *    ผลรวมของหน่วยกิตคูณด้วยเกรดแต่ละวิชา
           COMPUTE SUM-GRADE IN AVG-GRADE  = 
           SUM-GRADE IN AVG-GRADE  + EACH-COURSE-GRADE  IN AVG-GRADE 
      *    ค่าน้ำหนักทั้งหมด + กัน
           COMPUTE TOTAL-WEIGHT IN AVG-GRADE 
            = TOTAL-WEIGHT IN AVG-GRADE  + GRADE-WEIGHT  
           EXIT
       .

       005-CALCULATE-AVG-SCI-GRADE.
      *    หน่วยกิตคูณด้วยเกรดแต่ละรายวิชา
           COMPUTE EACH-COURSE-GRADE IN AVG-SCI-GRADE   
            = GRADE-EVALUATE * GRADE-WEIGHT 
      *    ผลรวมของหน่วยกิตคูณด้วยเกรดแต่ละวิชา
           COMPUTE SUM-GRADE IN AVG-SCI-GRADE  = 
           SUM-GRADE IN AVG-SCI-GRADE 
            + EACH-COURSE-GRADE  IN AVG-SCI-GRADE 
      *    ค่าน้ำหนักทั้งหมด + กัน
           COMPUTE TOTAL-WEIGHT IN AVG-SCI-GRADE 
            = TOTAL-WEIGHT IN AVG-SCI-GRADE  + GRADE-WEIGHT  
           
           EXIT
       .
       005-CALCULATE-AVG-CS-GRADE.
      *    หน่วยกิตคูณด้วยเกรดแต่ละรายวิชา
           COMPUTE EACH-COURSE-GRADE IN AVG-CS-GRADE    
            = GRADE-EVALUATE * GRADE-WEIGHT 
      *    ผลรวมของหน่วยกิตคูณด้วยเกรดแต่ละวิชา
           COMPUTE SUM-GRADE IN AVG-CS-GRADE  = 
           SUM-GRADE IN AVG-CS-GRADE 
            + EACH-COURSE-GRADE  IN AVG-CS-GRADE 
      *    ค่าน้ำหนักทั้งหมด + กัน
           COMPUTE TOTAL-WEIGHT IN AVG-CS-GRADE 
            = TOTAL-WEIGHT IN AVG-CS-GRADE  + GRADE-WEIGHT  
           EXIT
       .


       010-REPORT.
           OPEN  OUTPUT SCORE-FILE 
              MOVE  AVG-GRADE  IN AVG-GRADE  TO AVG-GRADE IN   
              SCORE-FILE   
              WRITE SCORE-DETAIL 
              MOVE  AVG-GRADE  IN AVG-SCI-GRADE  TO AVG-GRADE IN   
              SCORE-FILE   
              WRITE SCORE-DETAIL 
              MOVE  AVG-GRADE  IN AVG-CS-GRADE  TO AVG-GRADE IN   
              SCORE-FILE   
              WRITE SCORE-DETAIL 
           CLOSE SCORE-FILE 
           DISPLAY "AVG-GRADE: " AVG-GRADE IN AVG-GRADE
           DISPLAY "AVG-SCI-GRADE: " AVG-GRADE IN AVG-SCI-GRADE
           DISPLAY "AVG-CS-GRADE: " AVG-GRADE IN AVG-CS-GRADE

       .

       010-EXIT.
           EXIT
       .


           

       
       